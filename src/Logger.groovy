import java.text.SimpleDateFormat
import java.util.Date

/**
 * Possibility to log TRACE, DEBUG, INFO, WARN and ERROR levels
 * Simply call methods like info, debug etc. (case insensitive)
 * Possibility to set/change:
 * * logFile - location of a log file (default value:default.log)
 * * dateFormat - format of a date in log file(default value:dd.MM.yyyy;HH:mm:ss.SSS)
 * * printToConsole - whether a message should be printed to console as well (default value:false)
 * @author pavel.sklenar
 * def logger = new Logger()
 * logger.trace "Trace level test"
 * logger.debug "Debug level test"
 * logger.info "Info level test"
 * logger.warn "Warn level test"
 * logger.error "Error level test"
 * Source:  https://blog.pavelsklenar.com/simple-groovy-logger-without-any-logging-framework/
 */
class Logger {
    private File logFile = new File("Main.log")
    private String dateFormat = "yyyy-MM-dd HH:mm:ss.S"
    private boolean printToConsole = false

    /**
     * Catch all defined logging levels, throw  MissingMethodException otherwise
     * @param name
     * @param args
     * @return
     */
    def methodMissing(String name, args) {
        def messsage = args[0]
        if (printToConsole) {
            println messsage
        }
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat)
        String date = formatter.format(new Date())
        switch (name.toLowerCase()) {
            case "trace":
                logFile << "${date} TRACE ${messsage}\n"
                break
            case "debug":
                logFile << "${date} DEBUG ${messsage}\n"
                break
            case "info":
                logFile << "${date} INFO  ${messsage}\n"
                break
            case "warn":
                logFile << "${date} WARN  ${messsage}\n"
                break
            case "error":
                logFile << "${date} ERROR ${messsage}\n"
                break
            default:
                throw new MissingMethodException(name, delegate, args)
        }
    }
}
