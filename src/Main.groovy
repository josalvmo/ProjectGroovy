import java.text.SimpleDateFormat
import java.util.Date
import groovy.io.FileType //file library from Groovy

////////////////////////////////////////////////////////////////
//*
//*   MAIN CLASSS
//*   Author: Jose Alvarado
//*   Created Date: 2/22/2018
//*   Description: Runs as main app to search and replace a text within files
//*
///////////////////////////////////////////////////////////////
class Main{

    /*
	/ Main console app
    / Param1: Source files directory, program runs on subdirectories as well
    / Param2: text to be replaced
    / Param3: replacement text
    / Param4 (Optional): Ouput file Path to show files modified
    / Param5 (Optional): Backup Path, will store all files before modify them
	*/
    public static void main(String[] args){

        //Local variables to store arguments
        def String IN_SOURCE; // Mandatory 0
        def String IN_OLDTEXT; // Mandatory 1
        def String IN_NEWTEXT; // Mandatory 2
        def Boolean IN_REPLACE_FOLDER_NAMES //Mandatory 3
        def String[] IN_FILE_EXT // Optional 4
        def String IN_OUTPUTFILE; // Optional 5
        def String IN_BACKDIRECTORY; // Optional 6
        def logObj = new Logger();
        def outPutFileObj;

        logObj.debug "Program started $args";
      
        //Writes welcome script on screen
        welcome();
        //Validates how many arguments are received
        if (args.length <= 4){
            new Logger().error "Not enough args expected 4, found: #$args.length";
            println("Missing arguments, (found: ${args.length}), please make sure to indicate at least source path, old text and new text");
            println("Example: Main.groovy \"PATH\" \"SEARCH_TEXT\" \"TEXT_REPLACEMENT\" [\"OUTPUT_FILE\"] [\"BACKUP_DIRECTORY\"]");
            System.exit(0);
        }

        new Logger().debug "Assigns variables";
        //Assigns arguments to variables
        IN_SOURCE = args[0];
        IN_OLDTEXT = args[1];
        IN_NEWTEXT = args[2];

        if (args.length >= 4) {
            new Logger().debug "There is IN replace folder names option, value: '${args[3]}'";
            if (args[3].trim() == "True" || args[3].trim() == "1") {
                IN_REPLACE_FOLDER_NAMES = true;
            } else {
                IN_REPLACE_FOLDER_NAMES = false;
            }
        }

        //Validates if there is an optional file extentions
        if (args.length >= 5){
            new Logger().debug "There is an file extention list arg[4], value: '${args[4]}'";
            IN_FILE_EXT = args[4].split(";");
        }
        else{
            new Logger().debug "There is NO file extention list on arg[4]";
        }

        //Validates if there is an output file is sent in args
        if (args.length >= 6){
            new Logger().debug "There is an output file on arg[5], value: '${args[5]}'";
            IN_OUTPUTFILE = args[5];
            outPutFileObj = new FileAppender();
            outPutFileObj.setPathToFile(IN_OUTPUTFILE);
            outPutFileObj.AppendToFile("Program Started [DATE]");
        }
        else{
            new Logger().debug "There is NO output file sent on arg[5]";
        }

        //Validates if backup folder is sent in args
        if (args.length >= 7){
            new Logger().debug "There is a backup folder on arg[6], value: '${args[6]}'";
            IN_BACKDIRECTORY = args[6]; (args[6] != null) ? args[6] : "";
        }
        else{
            new Logger().debug "There is NO backup folder sent on arg[6]";
        }

        //checking for source directory validation
        if (IN_SOURCE?.trim()) {
            //Creates file object
            def sourceDirectory = new File(IN_SOURCE);
            //Validates if is a directory
            if (!sourceDirectory.isDirectory()){
                new Logger().debug "Source directory is not valid, value: '$IN_SOURCE'";
                println "The provided source directory '$IN_SOURCE' is NOT a valid directory."
                System.exit(0);
            }
        }
        else //in case of Source is empty
        {
            new Logger().debug "Source directory is not valid, value: '$IN_SOURCE'";
                println "The provided source directory '$IN_SOURCE' is NOT a valid directory."
                System.exit(0);
        }
        //checking for backup directory is not empty
        if (IN_BACKDIRECTORY?.trim()) {
            new Logger().debug "Backup directory shows not empty";
            //Creates file object
            def backupDirectory = new File(IN_BACKDIRECTORY);
            //Validates if is a directory
            if (!backupDirectory.isDirectory()){
                new Logger().debug "Backup directory is not valid, value: '$IN_BACKDIRECTORY'";
                println "The provided Backup directory '$IN_BACKDIRECTORY' is NOT a valid directory."
                System.exit(0);
            }
        }

        println("Searching for files!...");
        //Pulls list of files from path
        def listofFiles = FileManager.listFilesInFolder(IN_SOURCE,IN_FILE_EXT);
        def pathfromSource;

        //For each file found on directory and subdirectories, backup file and then do the replacement
        //Algorithm provides better control on targeting each file
        listofFiles.each {
            //Gets file's path relativelty from source path
            pathfromSource = it.path.replace(IN_SOURCE,"");
            print "FILE FOUND: '$pathfromSource'...";
            //Validates backup directory was sent as a parameter to proceed with backup file. Same directory tree as source
            if (IN_BACKDIRECTORY?.trim()){
                FileManager.copyFile(it.path, IN_BACKDIRECTORY+pathfromSource );
                print "Backup OK...."
                new Logger().debug "file: '$it.name' backup";
            }
            if (FileManager.replaceTextInFile(it.path,IN_OLDTEXT,IN_NEWTEXT)){
                new Logger().debug "file: '$it.name' - text replaced, from '$IN_OLDTEXT' to '$IN_NEWTEXT'";
                print "Replaced OK..."
                outPutFileObj.AppendToFile("file: '$pathfromSource' - text replaced, from '$IN_OLDTEXT' to '$IN_NEWTEXT'");
            } else {
                print "No content replacement needed..."
            }
            if (IN_REPLACE_FOLDER_NAMES) {
                def newPath = it.path.replace(IN_OLDTEXT,IN_NEWTEXT);
                if (it.path.toUpperCase() != newPath.toUpperCase()) {
                    if (FileManager.renameFile(it.path,newPath))
                        print "Text REPLACED in path..."
                    else {
                        print "Error renaming path..."
                    }
                }
                else {
                    print "No path rename needed.."
                }
            }
                
            println ""
        }
        //Calls to clean all empty directories
        FileManager.deleteEmptyDirs(IN_SOURCE);
        println("Deleting All Empty Folders.!");
        if(outPutFileObj!=null) outPutFileObj.AppendToFile("Program Finished [DATE]");
        println("Complete!");
    }

    //Shows welcome text
    static void welcome(){
        println();
        println("-----------------------------------------------------------------");
        println();
        println("       Git Library: https://gitlab.com/josalvmo/ProjectGroovy");
        println("       Author: Jose Alvarado");
        println("       Created Date: Feb/22/2018");
        println();
        println("-----------------------------------------------------------------");
        println();
    }
}

