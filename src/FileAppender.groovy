import java.text.SimpleDateFormat;
import java.util.Date;
import groovy.io.FileType; //file library from Groovy

////////////////////////////////////////////////////////////////
//*
//*   FILE APPENDER CLASS
//*   Author: Jose Alvarado
//*   Created Date: 2/26/2018
//*   Description: Appends text into defined file
//*
///////////////////////////////////////////////////////////////

class FileAppender {

    private String pathToFile;
    private File fileObj;
    private String dateFormat = "yyyy-MM-dd HH:mm:ss.S";
    private validFile = false;

    /*
	/ Sets path to file into String and validate existance
    / pPath: Source path of File
	*/
    def setPathToFile(String pPathtoFile){
        //checking for source directory validation
        if (pPathtoFile?.trim()) {
            //Creates file object
            fileObj = new File(pPathtoFile);
            //Validates if is a directory
            if ( !fileObj.isFile()){
                new Logger().debug "Output file does not exist or is invalid '$pPathtoFile'";
                println "Output file does not exist or is invalid text file '$pPathtoFile'. Output will be avoided"
            }
            else{
                validFile = true;
            }
        }
        else {
            new Logger().debug "Output file does not exist or is invalid '$pPathtoFile'";
            println "Output file does not exist or is invalid text file '$pPathtoFile'. Output will be avoided"
        }

    }

    /*
	/ Appends text into File
    / pText: Text to Append
	*/
    def AppendToFile(String pText){

        if (validFile){
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            String date = formatter.format(new Date());

            pText = pText.replace("[DATE]",date);

            fileObj << "${pText}\n"
        }
    }


}
