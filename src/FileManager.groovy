import groovy.io.FileType //file library from Groovy
import java.util.logging.Logger

////////////////////////////////////////////////////////////////
//*
//*   FILE MANAGER CLASS
//*   Author: Jose Alvarado
//*   Created Date: 2/22/2018
//*   Description: Handles all related file methods
//*
///////////////////////////////////////////////////////////////
class FileManager {
	/*
	/ Lists all files on given directory and subdirectories
    / pPath: Source path to search
    / Returns: List object of files in folder and subfolder
	*/
	def static List listFilesInFolder(String pPath, String[] pFileTypes){

		def dirlist = [];

        def dirPath = new File(pPath);

        dirPath.eachFileRecurse (FileType.FILES) { file ->
            if (pFileTypes.length > 0) {

                pFileTypes.each { type -> 
                    if (file.name.endsWith(type)) {
                    dirlist << file
                    }
                } 
            } else {
                dirlist << file
            }
        }
		return dirlist;
	}

    /*
    / Deletes all empty folders from given path
    */
    def static deleteEmptyDirs(String pPath) {
        def emptyDirs = []
        def dirPath = new File(pPath);

        //Lists all folders in path
        dirPath.eachFileRecurse (FileType.DIRECTORIES) { folder ->
            emptyDirs << folder
        }
        //Reverse order of folders in path and deletes them from child to parent if they have no files on it
        emptyDirs.reverse().each { dir -> 
            if ( dir.exists() && dir.directory && (dir.list() as List).empty  ) {
                //println "$dir ... deleted"
                dir.delete() 
            }
        }
    }
 
	/*
	/ Replace text on given file
    / pPath: Source path of file
    / pOldText: Text to search on file
    / pNewText: Text to replace on file based on oldfile
	*/
	def static replaceTextInFile(String pPath, String pOldText, String pNewText){
		//Variable to validate if replacement was made
        def fileReplaced = false;
        //Variable file path
		def orgFilePath = new File(pPath);
		//Gets file content and replace in runtime
	    def fileContent = orgFilePath.text.replace(pOldText, pNewText);
        //If content is different, sets tru to file replaced variable
        if (fileContent != orgFilePath.text)
            fileReplaced = true;
	    //Writes file
	    orgFilePath.text = fileContent
        //Returns variable
        return fileReplaced;
	}

	/*
	/ Copy all files from one destination to another
    / pSourceFilePath: Source file path
    / pDestinationPath: Destination file path
	*/
	def static copyFile(String pSourceFilePath, String pDestinationPath){
        //Created AntBuilder object
        def antObj = new AntBuilder();
        //Added to hide any imput from AntBuilder when copying file and disrupt application output
        antObj.project.buildListeners[0].messageOutputLevel = 0;
        //Defines File objects for Source and Destination
        def sourceFilePath = new File(pSourceFilePath);
        def destinationPath = new File(pDestinationPath);
        //Copies file
        antObj.copy( file: sourceFilePath.absolutePath, tofile : destinationPath.absolutePath )
	}

    /*
    / Replace file name and path
    / Delete original directory if no files are left
    */
    def static boolean renameFile(String pSourceFilePath, String pDestinationPath ) {

        def file = new File(pSourceFilePath);
        def parent = file.getParentFile();
        def newFile = new File(pDestinationPath);
        //Creates new directories if existed
        newFile.getParentFile().mkdirs();
        //Copies content
        newFile << file.text

        if (newFile.exists() && file.length() > 0) {
            //Deletes old file
            file.delete();
            return true;
        } else {
            return false;
        }
    }
}
