
## Groovy Project

Search and replace within all files starting in a given directory. Assumes files as text

Refer to Project Plan for more details on task breakdown

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See How To Run for notes on how to deploy the project on a live system.

### Prerequisites to Install Groovy in Windows System

1. Download and install latest version of [Groovy](http://groovy-lang.org/download.html) (Windows installer preferred)
2. Setup Environment Variables 
    * Set GROOVY_HOME = Groovy Program Files Path, Example: C:\Program Files (x86)\Groovy\Groovy-2.6.0
    * Add to PATH = %GROOVY_HOME%\bin
    * Set JAVA_HOME to Pertinent JDK, Example C:\Program Files\Java\jdk1.8.0_161
    
How to set Windows Environment Variables? Click [here](https://www.java.com/en/download/help/path.xml)

Installation assumes you have Java SDK Installed and GIT Knowledge
 
### How to Run

Code is intended to be compiled and run at same time (Current directory \src)

```console
groovy Main.groovy Param1 Param2 Param3 [Param4] [Param5]
```

* Param1: Source files directory, program runs on subdirectories as well
* Param2: text to be replaced
* Param3: replacement text
* Param4: "True" if intent to replace path
* Param5: List of file extentions, ";" separated
* Param6 (Optional): Ouput file Path to show files modified
* Param7 (Optional): Backup Path, will store all files before modify them

Log will be automatically generated at \src\Main.log

## Example

Input:

BATCH Current directory \src

```console
$ groovy Main.groovy "C:\Users\j.alvarado.moreira\Documents\REPLACE" "searchtechnologies" "accenture" "True" ".java;" "C:\Users\j.alvarado.moreira\Documents\Projects\ProjectGroovy\src\output.txt"
```
Output:

```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Searching for files!...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreConnection.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreFetcher.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreGroupExpansion.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreItemType.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreQueryManager.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreRAP.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\main\java\com\searchtechnologies\aspire\components\AzureDataLakeStoreSourceInfo.java'...Replaced OK...Text REPLACED in path...
FILE FOUND: '\aspire-azure-datalake-connector\src\test\java\com\searchtechnologies\aspire\components\TestAzureDataLakeStoreScanner.java'...Replaced OK...Text REPLACED in path...
Complete!


```

## Algorithm & Coding Methodology

Basically the main process searches each file on source folder and on its subdirectories. For each file found, it creates a backup on backup directory (if applicable) and then proceed with the text search and pertinent replace. 

I could do a bulk copy to backup folder and then do the replacement, but due to efficiency to do backup and replace at same time.

Groovy programming seems very easy to use and very similar to Java, and even though I was not actively Java programmer, my experience with C# helped a lot on writing such code.

## Test Cases

### Test Case 1 : Missing Parameters

Inputs
```console
groovy Main.groovy
```
```console
groovy Main.groovy ""
```
```console
groovy Main.groovy "" ""
```
Output

Process should just do an alert and keep files (if existed in the user intent) in original state
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Date: Feb/22/2018

-----------------------------------------------------------------

Missing arguments, (found: 0), please make sure to indicate at least source path, old text and new text
Example: Main.groovy "PATH" "SEARCH_TEXT" "TEXT_REPLACEMENT" ["OUTPUT_FILE"] ["BACKUP_DIRECTORY"]
```

### Test Case 2 : Empty or invalid source path

Inputs
```console
groovy Main.groovy "" "" ""
```
```console
groovy Main.groovy "" "Hello" "World"
```
```console
groovy Main.groovy "T:\folder" "Hello" "World"
```
Output

Process should just do an alert and keep files (if existed in the user intent) in original state
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

The provided source directory 'T:\folder' is NOT a valid directory.
```


### Test Case 3: Empty Output File

Inputs
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" ""
```
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" "T:\file.txt"
```

Output

Text in files should be replaced, but output file will not be generated (as parameter is optional)
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Output file does not exist or is invalid text file ''. Output will be avoided
Searching for files!...
FILE FOUND: '\test\test2.txt'...Replaced OK...
FILE FOUND: '\test\testttt\testt.txt'...Replaced OK...
FILE FOUND: '\test1.txt'...Replaced OK...
FILE FOUND: '\testttttttt\test\test2.txt'...Replaced OK...
FILE FOUND: '\testttttttt\test\testtt\testtt.txt'...Replaced OK...
FILE FOUND: '\testttttttt\testttt.txt'...Replaced OK...
Complete!
```

### Test Case 4: Empty backup path

Inputs
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt" ""
```
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt"
```
Output

Text in files should be replaced but source files not backed up.
Output file will be written at: "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt"
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Searching for files!...
FILE FOUND: '\test\test2.txt'...Replaced OK...
FILE FOUND: '\test\testttt\testt.txt'...Replaced OK...
FILE FOUND: '\test1.txt'...Replaced OK...
FILE FOUND: '\testttttttt\test\test2.txt'...Replaced OK...
FILE FOUND: '\testttttttt\test\testtt\testtt.txt'...Replaced OK...
FILE FOUND: '\testttttttt\testttt.txt'...Replaced OK...
Complete!
```

### Test Case 5: Normal behaviour

Inputs
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files_backup"
```
Output

Text in files should be replaced and original files backup up
Output file will be written at: "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt"
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Searching for files!...
FOUND: '\Curso Excel.xlsx'...Backup OK....Replaced OK...
FOUND: '\test\test2.txt'...Backup OK....Replaced OK...
FOUND: '\test1.txt'...Backup OK....Replaced OK...
Complete!
```

### Test Case 6: Files with long name, Folder long names, Doc Files, PDF Files

Inputs
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "World" "World25621" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files_backup"
```
Output

Text in files should be replaced and original files backup up, output list of files will be written in Output.txt
Files that are not in text format, may be corrupted - Expected
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Searching for files!...
FILE FOUND: '\test\test2.txt'...Backup OK....Replaced OK...
FILE FOUND: '\test\testttt\testt.txt'...Backup OK....Replaced OK...
FILE FOUND: '\test1.txt'...Backup OK....Replaced OK...
FILE FOUND: '\testttttttt\test\test2.txt'...Backup OK....Replaced OK...
FILE FOUND: '\testttttttt\test\testtt\testtt.txt'...Backup OK....Replaced OK...
FILE FOUND: '\testttttttt\testttt.txt'...Backup OK....Replaced OK...
Complete!
```

### Test Case 7: Text Not found in Search

Inputs
```console
groovy Main.groovy "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files" "z" "World25621" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\src\Output.txt" "C:\Users\Josalvmo\Documents\GitProjects\ProjectGroovy\files_backup"
```
Output

If searching text was not found, should not be any changes on original file, however backup process still be executed if parameter is sent
```
-----------------------------------------------------------------

       Git Library: https://gitlab.com/josalvmo/ProjectGroovy
       Author: Jose Alvarado
       Created Date: Feb/22/2018

-----------------------------------------------------------------

Searching for files!...
FILE FOUND: '\test\test2.txt'...Backup OK....Text Not Found.
FILE FOUND: '\test\testttt\testt.txt'...Backup OK....Text Not Found.
FILE FOUND: '\test1.txt'...Backup OK....Text Not Found.
FILE FOUND: '\testttttttt\test\test2.txt'...Backup OK....Text Not Found.
FILE FOUND: '\testttttttt\test\testtt\testtt.txt'...Backup OK....Text Not Found.
FILE FOUND: '\testttttttt\testttt.txt'...Backup OK....Text Not Found.
Complete!
```

## Built With & Libraries

* [Groovy](http://groovy-lang.org/download.html)
* [AntBuilder](http://docs.groovy-lang.org/latest/html/documentation/ant-builder.html)
* [Logger](https://blog.pavelsklenar.com/simple-groovy-logger-without-any-logging-framework/)

## Versioning

  * Release 1.0: MVP

## Project Plan

**User Story 1:** Develop simple app in Groovy to search and replace within all files starting in a given directory 		

|  **Task No.**	|	**Task Description**|	**Estimated (H)**	|**Actual (H)**|**Delta**|**Resources**|**Predecessor**	|
| :----------: | :------                | :------------:    | :-----------: | :------:  | :------------: |:------------: 
|1|Develop Project Plan|0.5|0.5|0|Jose Alvarado||
|2|Setup Groovy Environment|2|2|0|Jose Alvarado|1|
|3|Setup GitLab Environment|0.5|0.5|0|Jose Alvarado|2|
||Code Development||||||
|4|File Manager Class|3|4|1|Jose Alvarado|3|
|5|Main Class|2|3|1|Jose Alvarado|4|
|6|Logging Class|2|0.5|-1.5|Jose Alvarado|5|
|7|File Appender Class|2|2|0|Jose Alvarado|6|
|8|Validation & UX Experience|2|3|1|Jose Alvarado|7|
|9|Write Git Readme File|1|1|0|Jose Alvarado|8|
||**TOTAL TIME**|15|16.5|1.5|||

	
**Sprint 1:** *Develop MVP to execute a search and replace within files with optional backup option*	

**Release 1.0:** Minimum Viable Product	

For updated project plan, please refer to [ProjectPlan.xlsx]("https://gitlab.com/josalvmo/ProjectGroovy/blob/master/ProjectPlan.xlsx") file on main directory

## Authors

* **Jose Alvarado** - *Initial work* - [josalvmo](https://gitlab.com/josalvmo/)

## License

Open source, feel free to use and share

## Acknowledgments

This project is an example of my personal Portfolio. 
